import React from 'react'
import styled from 'styled-components'

const TagContainer = styled.div`
  display: inline-block;
`
const Tag = styled.div`
  
  display: inline-flex;
  justify-content: space-between;
  align-items: center;
  padding: 12px;
  margin-right: 15px;
  margin-top: 15px;
  margin-bottom: 10px;
  height: 43px;
  border-radius: 3px;
  border: solid 0.6px #b6cbff;
  background-color: #f5f7ff;

  font-family: Helvetica, sans-serif ;
  font-size: 16px;
  font-weight: 500;
  color: #201d5e;
`

const CloseButton = styled.button`
  width:20px;
  height:20px;
  border:none;
  margin-left:10px;
  display: inline-block;
  cursor: pointer;
  color: #201d5e;
  background-color: transparent;
  transition: 250ms;

  &:hover {
    background-color: darken(#f5f7ff, 4%);
    transform: scale(1.2);
  }
`

const Tags = (props) => {
  return (
    <TagContainer>
      <Tag>
        {props.label ? props.label : 'demo tag'}
        <CloseButton className="mdi mdi-close" />
      </Tag>
    </TagContainer>
  )
}

export default Tags