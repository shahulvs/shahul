import React from 'react'
import Slider from '@material-ui/core/Slider';
import styled from 'styled-components'

const InputCont = styled.div`
  display: flex;
  align-items: center;
  height: 60px;
  width: 200px;

  & .MuiSlider-thumb{
      background-color: #201d5e ;
      width: 26px;
      height: 26px;
      margin-top: -9px;

      & span{
        transform: unset;
        border-radius: unset;
      }
      
     & > span { top: 40px }

     & > span >span{
      min-width: 49px;
      height: 43px;
      border-radius: 3px;
      border: solid 0.6px #b6cbff;
      background-color: #f5f6ff;
      
      & span{
        font-family: helvetica;
        color: #b6cbff;
        font-size: 16px;
        font-weight: 500;
        color: #201d5e;
      }
     }
    }
   & .PrivateValueLabel-circle-147{
      background-color: #ff8896;
    }
   & .MuiSlider-rail{
      height: 8px;
      border-radius: 6px;
      background-color: #f3f3f3;
    }
   & .MuiSlider-track{
      height: 8px;
      border-radius: 6px;
      background-color: #ff8896 ;
    }
`

function valuetext(value) {
  return `${value}°C`;
}

const RangeSlider = ({ sliderVal }) => {

  const [value, setValue] = React.useState(sliderVal || [10, 60]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <InputCont>
      <Slider
        value={value}
        onChange={handleChange}
        valueLabelDisplay="on"
        aria-labelledby="range-slider"
        getAriaValueText={valuetext}
      />
    </InputCont>
  )
}

export default RangeSlider

