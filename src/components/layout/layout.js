/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"

import Header from "./header"
import Footer from "./footer"
import "./layout.scss"
import styled from "styled-components"

const ContentCont = styled.div`
  flex-direction: ${ props => props.row ? 'row' : 'column'};
  max-width: unset; 
  flex-wrap: wrap;
`

const Layout = ({ children, row }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  return (
    <main className='root-cont'>
      <Header siteTitle={data.site.siteMetadata.title} />

        <ContentCont row={row} className='content-cont'>
          {children}
        </ContentCont>

      <Footer />
    </main>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
