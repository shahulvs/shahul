import React from 'react'
import styled from 'styled-components'

const StyledLink = styled.a`
  
  /* font-family: $primaryfont; */
  font-size: 16px;
  font-weight: 500;
  color: #201D5E;
  text-align: center;
  
  &:hover {
    color: darken(#201D5E, 20%);
  }
  &::after {
    content: '';
    display: block;
    width: auto;
    height: .5px;
    border: solid .5px #ff8896;
  }
`
const TextLink = (props) => {
  return (
    <StyledLink
    href={props.href} 
    >
    {props.label}
  </StyledLink>
    )
}

export default TextLink
