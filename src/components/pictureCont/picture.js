import React from 'react'
import play from './assets/play.svg'
import tick from './assets/tick.svg'
import add from './assets/add.svg'
import styled from 'styled-components'

const MarginCont = styled.div`
  padding: 0 54px;
@media only screen and (max-width:600px){
  width: 100%;
  padding: 0 16px;
}
`
const ContentsCont = styled.main`
  margin: 0 -20px;
  padding-bottom: 36px;
  display: flex;
  flex-wrap: wrap;
  justify-content: flex-start;
@media only screen and (max-width:600px){
  margin: unset;
  width:100%;
  justify-content: space-evenly;
}
`
const ImageCont = styled.div`
margin:19px;
width: 264px;
height: 264px;
object-fit: contain;

object-fit: cover;
position: relative;
overflow: hidden;
display: inline-block;
border-radius: 12px;
transition: .3s ease;

&:before {
  content: '';
  position: absolute;
  top: 0;
  left: 0;
  transition: .5s ease;
  width: inherit;
  height: inherit;
  border-radius: inherit;
  opacity: 0.46;
  background-image: linear-gradient(to bottom, #201d5e,
                    rgba(27, 27, 27, 0));
  z-index: 1;
}

&:hover {
  /*  transform: scale(2); */
  // transform: translate(0, -4px);
  box-shadow: 0 0 40px 0 rgba(160, 156, 225, 0.45);
  z-index: 3;

  &::before {
    opacity: .1;
  }
}

@media only screen and (max-width:600px){
  width: 134px;
  height: 134px;
  margin: 5px;
}
`
const ImgPost = styled.img`
z-index: 0;
display: block;
width: 100%;
`
const TickBtn = styled.button`
outline: none;
background-color: unset;
position: absolute;
top: 10.8px;
right: 10px;
z-index: 2;
cursor: pointer;
opacity: .8;
border: none;

&:hover {
  opacity: 1;
}

@media only screen and (max-width:600px){
  height: 40px;
        width: 40px;
}
`
const BtnImg = styled.img`
@media only screen and (max-width:600px){
  height: inherit;
          width: inherit;
}

`
const PlayBtn = styled.div`
position: absolute;
top: 50%;
left: 50%;
transform: translate(-50%, -50%);
z-index: 2;
cursor: pointer;
opacity: .8;

&:hover {
  opacity: 1;
}
`
const PlayBtnImg = styled.img``


const Picture = ({ posts, setPosts }) => {
  return (
    <MarginCont className='marginCont'>
      <ContentsCont className='contentsCont unset'>
        {
          posts.map((post, index) => <Post key={index} {...{ index, post, posts, setPosts }} />)
        }
      </ContentsCont>
    </MarginCont>
  )
}

const Post = ({ index, post, posts, setPosts }) => {
  return (
    <ImageCont className='imageCont gradient'>

      <ImgPost className='imgPost' src={post.imgUrl} alt='img' />

      <TickBtn
        className='tickBtn'
        onClick={() => {
          const temp = [...posts];
          temp[index].selectedImg = !(temp[index].selectedImg);
          setPosts(temp);
        }} >
        <BtnImg src={post.selectedImg ? tick : add} alt='button' />
      </TickBtn>

      {
        post.isVideo ?
          <PlayBtn className='playBtn'>
            <PlayBtnImg src={play} alt='play' />
          </PlayBtn>
          : null
      }

    </ImageCont>
  )
}

export default Picture