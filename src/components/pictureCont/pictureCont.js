import React,{useState} from 'react'
import Picture from './picture'
import './pictureCont.scss'
import styled from 'styled-components'

const Box = styled.div`
  width: 1280px;
  margin: auto;
  background-color: white;
  border-radius: 10px;
  margin-top: 70px;
  margin-bottom: 0;

  @media only screen and (max-width:600px){
    margin-top: unset;
    width:100%;
  }
`
const Header = styled.section`
display: flex;
padding: 0 54px;
margin: auto;
justify-content: space-between;
align-items: center;
@media only screen and (max-width:600px){
  flex-direction: column;
  justify-content: unset;
  padding: 10px;
}
`
const Aside = styled.aside`
@media only screen and (max-width:600px){
  margin-left: 20px;
}
`
const Heading = styled.h2`
font-family: Helvetica, sans-serif;
font-size: 26px;
margin-top: 54px;
margin-bottom: 26px;
@media only screen and (max-width:600px){
  font-size: 15px;
  margin-top: 20px;
  margin-bottom: 20px;
}
`
const SubHeading = styled.p`
font-family: Helvetica, sans-serif;
font-size: 16px;
margin-bottom: 50px;
@media only screen and (max-width:600px){
  font-size: 12px;
  margin-bottom: 20px;
}
`
const PrimaryBtn = styled.button`
border: none;
background-color: #ff6d7f;
width: 260px;
height: 67px;
border-radius: 5px;
color: white;
font-size: 16px;
@media only screen and (max-width:600px){
  width: 120px;
    height: 40px;
    font-size: 14px;
    margin-bottom: 10px;
}
`

const PictureCont = (props) => {

 const { h, sH, myPosts } = props
 const [posts,setPosts] = useState([...myPosts])

  return (
    <Box>
      <Header>
        <Aside>
          <Heading>{h}</Heading>
          <SubHeading>{sH}</SubHeading>
        </Aside> 
        <PrimaryBtn>Next</PrimaryBtn>
      </Header>
      <Picture {...{posts,setPosts}}/>
    </Box>
  )
}

export default PictureCont
