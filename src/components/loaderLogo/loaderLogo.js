import React from 'react'
import styled,{keyframes} from 'styled-components';

 const LoaderCont = styled.div`
   display: flex;
   justify-content: center;
   align-items: center;
   height: 20px;
   width: 20px;
 `
  const rotate = keyframes`
  from {
      transform: rotate(0deg);
  }
  to {
      transform: rotate(359deg);
  }
`
 const Loading = styled.svg`
   animation: ${rotate} 2s infinite linear;
   display: inline-block;
   width: inherit;
   height: inherit;
 `

const LoaderLogo = () => {
 return <LoaderCont>

    <Loading xmlns="http://www.w3.org/2000/svg" width="31" height="31" viewBox="0 0 31 31">
      <g fill="none" fillRule="evenodd" transform="translate(1 1)">
         <circle cx="14.5" cy="14.5" r="14.5" stroke="#201D5E" strokeWidth="1.584"/>
         <path fill="#FF8896" d="M25.338 15.406c-.46 5.582-5.137 9.969-10.838 9.969-5.7 0-10.377-4.387-10.838-9.969h21.676z"/>
      </g>
   </Loading>

 </LoaderCont>
}

export default LoaderLogo