import React from 'react'
import styled from 'styled-components'

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';

import loginIcon from './assets/loginIcon.svg'
import loginBlue from './assets/loginBlue.svg';

import logo from './assets/footer_logo.svg'
import insta from './assets/insta.svg'
import close from './assets/x.svg'

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`
const LoginCont = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding:0 36px;
  position: relative;
  @media only screen and (max-width: 460px){
    padding:0 6px;
  }
`
const CloseBtn = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  border: none;
  background-color: #201d5e ;
  width: 35px;
  height: 35px;
  border-radius: 50px;
  padding: 11px;
  position: absolute;
  top: 0;
  right: 0;
  margin: 20px 20px 0 0 ;
  transition: 300ms;

  &:hover{
    background-color: lighten(#201d5e,10%);
  }
`
const LogoCont = styled.div`
  margin-top: 74px;
`
const Heading = styled.div`
  margin-top: 44px;
  font-family: helvetica;
  @media only screen and (max-width: 460px){
    font-size:14px;
  }
`
const SubHeading = styled.div`
  margin-top: 11px;
  max-width: 427px;
  text-align: center;
  line-height: 1.71;
  color: #5b5b5b;
  font-family: helvetica;
  font-size: 14px;
  font-weight: 300;
  @media only screen and (max-width: 460px){
    font-size:12px;
  }
`
const LoginBtn = styled.div`
  cursor: pointer;
  font-family: helvetica;
  margin-top: 48px;
  width: 352px;
  height: 54px;
  border-radius: 6px;
  background-color: #ff8896;
  margin-bottom: 74px;
  transition: 300ms;

  &:hover{
  background-color: #fb6d7e;
  transform: translate(0,-4px);
  box-shadow: 0 0 20px 0 rgba(160, 156, 225, 0.25);
  }
  @media only screen and (max-width: 460px){
   width:80%;
  }
`

const ButtonLink = styled.a`
  width: 100%;
  text-decoration: none;
  color: #263646;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  color: white;
  @media only screen and (max-width: 460px){
    font-size: 14px;
  }
`
const Img = styled.img`
  padding-right: 20px;
  @media only screen and (max-width: 460px){
    padding-right: 10px;
  }
`

const ModalLogin = ({ cN, bg, label, href, heading, subHeading}) => {

  // cN classname for diffrent styled buttons
  // bg true then icon in button changes accordingly

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => setOpen(true)

  const handleClose = () => setOpen(false)

  return (
    <Wrapper>
      <Button onClick={handleClickOpen}>
        {cN === 'teritaryBtn' ?
          <>
            <img className='xl' src={bg ? loginBlue : loginIcon} alt='' />
            <img className='xs' src={loginBlue} alt='' />
          </>
          : null}

        {label ? label : 'Click Here' }
      </Button>
      <Dialog open={open} onClose={handleClose}>
        <LoginCont>
          <CloseBtn
            onClick={handleClose}>
            <img
              src={close}
              alt='close'
              title='close'
            />
          </CloseBtn>
          <LogoCont>
            <img src={logo} alt='Influensure' />
          </LogoCont>
          <Heading>
            {heading}
        </Heading>
          <SubHeading>
            {subHeading}
        </SubHeading>
          <LoginBtn className='loginBtn'>
            <ButtonLink href={href} title='authentication'>
              <Img
                src={insta}
                alt='Instagram Logo' />
              Login with Instagram
            </ButtonLink>
          </LoginBtn>
        </LoginCont>
      </Dialog>
    </Wrapper>
  )
}

export default ModalLogin 
