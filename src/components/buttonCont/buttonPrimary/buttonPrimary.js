import React from 'react'
import styled from 'styled-components'

const ButtonPrimary = (props) => {

  const ButtonPrimary = styled.button`
    outline: none;
    border: none;
    margin: 0 10px;
    padding: 14px;
    height: 50px;
    min-width: 180px;
    border-radius: 4px;
    background-color: #ff8896;
    display: flex;
    text-decoration: none;
    align-items: center;
    justify-content: center;
    outline: none;
    color: white;
    font-family: inherit;
    transition: 300ms;
    font-family: helvetica, sans-serif;
    text-transform: capitalize;

    &:hover {
      background-color: #ff6d7f;
    }

    &:disabled {
      opacity: .6;
      pointer-events: none;
    }

    @media only screen and (max-width: 600px){
      min-width: 140px;
    }
  `
  const A = styled.a`
    color:inherit;
  `
  return (
    <A href={props.href} title={props.title}>
      <ButtonPrimary>
          {props.children ? props.children : 'Primary button' }
      </ButtonPrimary>
    </A>
  )
}

export default ButtonPrimary
