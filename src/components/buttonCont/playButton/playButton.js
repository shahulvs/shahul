import React from 'react'
import styled from 'styled-components'

const PlayBtn = styled.button`
  width: 40px;
  height: 40px;
  border: unset;
  outline: none;
  opacity: 0.8;
  border-radius: 50%;
  &:hover Path{
    fill: #ff8896;
  }
`

const Path = styled.path`
  transition : 200ms;
`
const PlayButton = () => {
  return (
    <PlayBtn>
      <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 62 62">
          <g fill="none" fillRule="evenodd">
              <circle cx="31" cy="31" r="31" fill="#FFF" opacity=".543"/>
              <Path fill="#201D5E" d="M38.81 31.538L26.34 39.244a1 1 0 0 1-1.526-.85V22.98a1 1 0 0 1 1.526-.85l12.471 7.706a1 1 0 0 1 0 1.702z"/>
          </g>
      </svg>
    </PlayBtn>
  )
}

export default PlayButton
