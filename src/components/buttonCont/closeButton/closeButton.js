import React from 'react'
import styled from 'styled-components'

const CloseBtn = styled.button`
  margin: 0 10px;
  height: 40px;
  width : 40px;
  border: unset;
  outline: none;
  border-radius: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #373295;
  transition : 200ms background-color;
  &:hover{
    background-color: #4d47c9;
  }
`
const CloseButton = () => {
  return (
    <CloseBtn>
      <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 13 13">
          <g fill="none" fillRule="evenodd" stroke="#FFF" strokeLinecap="round" strokeLinejoin="round" strokeWidth="1.8">
              <path d="M10.862 0L0 10.862M0 0L10.862 10.862" transform="translate(1.069 1.069)"/>
          </g>
      </svg>
    </CloseBtn>
  )
}

export default CloseButton
