import React from 'react'
import styled from 'styled-components'

const ButtonSecondary = (props) => {

  const ButtonSecondary = styled.button`
    outline: none;
    border: none;
    margin: 0 10px;
    padding: 14px;
    height: 50px;
    min-width: 180px;
    border-radius: 4px;
    background-color: transparent;
    border: 1px solid ${props.color ? props.color : 'rebeccapurple'};;
    display: flex;
    text-decoration: none;
    align-items: center;
    justify-content: center;
    outline: none;
    color: ${props.color ? props.color : '#201d5e'};
    font-family: inherit;
    transition: 300ms;
    font-family: helvetica, sans-serif;
    text-transform: capitalize;
    
    &:hover{
      border-color:  #ff8896;
      color:  #ff8896;
    }

    @media only screen and (max-width: 600px){
      min-width: 140px;
    }
  `
    const A = styled.a`
    color:inherit;
  `

  return (
    <A href={props.href} title={props.title}>
      <ButtonSecondary>
        {props.children ? props.children : 'Secondary Button' }
      </ButtonSecondary>
    </A>
  )
}

export default ButtonSecondary
