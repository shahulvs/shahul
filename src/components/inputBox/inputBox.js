import React from 'react'
import styled from 'styled-components'

export const ErrorBtn = () => {
  return(
  <svg xmlns="http://www.w3.org/2000/svg" width='50%' height="50%" viewBox="0 0 13 13">
    <g fill="none" fillRule="evenodd" stroke="#FFF" strokeLinecap="round" strokeLinejoin="round" strokeWidth="1.8">
        <path d="M10.862 0L0 10.862M0 0L10.862 10.862" transform="translate(1.069 1.069)"/>
    </g>
  </svg>
)

}

export const TickBtn = () => {
  return (
  <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 51 51">
    <g fill="none" fillRule="evenodd" transform="translate(.414 .721)">
        <ellipse cx="24.947" cy="24.973" fill="lightgreen" rx="24.947" ry="24.973"/>
        <path stroke="#FFF" strokeLinecap="round" strokeLinejoin="round" strokeWidth="3.9" d="M15.586 27.694l5.784 4.585 13.216-14"/>
    </g>
  </svg>
  )
}

const InputCont = styled.div`
  position:relative;
`

const InputText = styled.input`
  padding-left: 16px;
  font-family: Helvetica, sans-serif;
  font-size: 16px;
  font-weight: 300;
  color: #201d5e;
  width: 100%;
  height: 54px;
  border-radius: 4px;
  border: 1px solid grey;
  transition: 200ms;
  border: ${ props => props.valid ? '1px solid lightGreen' : '1px solid red'};

  position: relative;

  &:focus{
    outline: ${ props => props.valid ? 'none' : 'none'};
  }
`
const IsValidIconCont = styled.button`
  background: none;
  outline:none;
  border: none;
  position:absolute;
  top: 50%;
  left: 100%;
  width: 16px;
  height: 16px;
  transform: translate(-200%, -50%);
`
const IsValidIcon = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
  transition: 200ms;
  background-color:${props => props.valid ? null : '#f44336'};
`

const InputBox = (props) => {
  return (
    <InputCont>
      <InputText placeholder='Write Here' valid={props.valid} />
      <IsValidIconCont>
        {
          // <IsValidIcon src={props.valid ? (props.error ? <ErrorBtn/> : <TickBtn/>) : null} alt='pin' />
          <IsValidIcon valid={props.valid}>

           { props.valid ? <TickBtn/> : <ErrorBtn/> }

          </IsValidIcon>
        }
      </IsValidIconCont>
    </InputCont>
  )
}

export default InputBox
