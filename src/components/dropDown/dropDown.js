import React from 'react';
import {Button, Menu , MenuItem } from '@material-ui/core';
import {navigate} from 'gatsby'
import styled from 'styled-components'

const InputCont = styled.div`
  position:relative;
`
const DropDown = ({ menuProps }) => {

    //const [button, links, labels] = menuProps;

    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = event => {
      setAnchorEl(event.currentTarget);
    };
  
    const handleClose = () => {
      setAnchorEl(null);
    };

    const StyledSvgDownArrow = styled.svg`
      margin-left: 10px;
    `
  
  return <>
    <InputCont>
        <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
          {menuProps.button}      
          <StyledSvgDownArrow xmlns="http://www.w3.org/2000/svg" width="10" height="6" viewBox="0 0 10 6">
            <path fill="none" fillRule="evenodd" stroke="#FF8896" strokeLinecap="round" strokeLinejoin="round" strokeWidth="1.5" d="M69 5L72.861 9 76.722 5" transform="translate(-68 -4)"/>
          </StyledSvgDownArrow>
        </Button>
    </InputCont>

    <Menu
      id="simple-menu"
      anchorEl={anchorEl}
      keepMounted
      open={Boolean(anchorEl)}
      onClose={handleClose}
    >
      {
        menuProps.links.map( (link,index) => 
          <MenuItem 
          key={index}
          onClick={ () => {
            navigate(link) 
           } }
          >
            {menuProps.labels[index]}
          </MenuItem>
        )
      }
    </Menu>
  </>
}

export default DropDown
