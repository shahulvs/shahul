import React from 'react'
import {Radio,FormControl,FormLabel, FormControlLabel, RadioGroup} from '@material-ui/core';
import styled from 'styled-components';

const StyledRadioGroup = styled(RadioGroup)`
  flex-direction: row !important;
`

const StyledRadio = styled(Radio)`
  & .MuiSvgIcon-root{
    fill:#201d5e;
  }
`

const RadioButton = ({options, groupName, groupLabel}) => {

  const [value, setValue] = React.useState('yes');

  const handleChange = (event) => {
    setValue(event.target.value);
  };

  return (
    <div>

    <FormControl component="fieldset">
      <FormLabel component="legend">Demo</FormLabel>
      <StyledRadioGroup aria-label="demo" name="demo" value={value} onChange={handleChange}>
        <FormControlLabel value="yes" control={<StyledRadio />} label="Yes" />
        <FormControlLabel value="no" control={<StyledRadio />} label="No" />
      </StyledRadioGroup>
    </FormControl>


      {/* <StyledRadioGroup 
      aria-label={groupLabel} 
      name={groupName} 
      value={value} 
      onChange={handleChange}
      >
        {
          options.map( (option,index)  => {
            return (
            <FormControlLabel
            key={index} 
            value={option.value}
            control={<StyledRadio />}
            label={option.label} />
            )
          })
        }
      </StyledRadioGroup> */}
    </div>
  )
}

export default RadioButton
