import React from 'react'
import styled from 'styled-components'


import Autocomplete from '@material-ui/lab/Autocomplete';
import {TextField} from '@material-ui/core';

const InputCont = styled.div`
  width: 200px;
  border-radius: 4px;
  border: 1px solid lightgray;
  transition: 200ms;

  & .MuiChip-root{
    border-radius: unset;
  }
`
/*
  font-family: Helvetica,sans-serif;
  font-size: 16px;
  font-weight: 300;
  color: #201d5e;
*/
const StyledAutocomplete = styled(Autocomplete)`
  position: relative;
  height: 100%;

  & .MuiFormControl-fullWidth{
    height: 100%;
    & .MuiAutocomplete-inputRoot[class*="MuiInput-root"]{
      min-height: 54px;
      & .MuiAutocomplete-input {
        padding: unset;
        padding-left: 10px;
      }
    }

  }
`

const InputText = styled(TextField)`
  margin-bottom: unset;

  padding-left: 16px;
  font-family: Helvetica, sans-serif;
  font-size: 16px;
  font-weight: 300;
  color: #201d5e;

  border-radius: 4px;
  border: 1px solid grey;
  transition: 200ms;
  border: ${ props => props.valid ? '1px solid lightGreen' : '1px solid red'};

  position: relative;

  &:focus{
    outline: ${ props => props.valid ? 'none' : 'none'};
  }
`

const StyledSvg = styled.svg`
  position: absolute;
  top: 50%;
  right: 0;
  width: 14px;
  transform: translate(-15px, -50%);
`

const DropDown = (props) => {

  const districts = [
    { label: 'Tamil Nadu' },
    { label: 'Kerala' },
    { label: 'Karnataka' },
    { label: 'Andra' },
    { label: 'Telungana' },
    { label: 'Asam' },
    { label: 'Delhi' }
  ];

  const [locations, setLocations] = React.useState([]);

  const onChangeHandle = (e,n,v,fn)=>{
    let current = e.currentTarget.innerText;
    let isExist = v.some((val)=>{ return val.label === current});
    if(!isExist){
      fn(n);
    }else{
      let filtered = [...v].filter(f=>f.label !== current);
      fn(filtered)
    }
  };
  
  return (
    <InputCont>

      <StyledAutocomplete
        multiple
        disableClearable={true}
        options={districts}
        getOptionLabel={option => option.label}
        freeSolo
        value={locations}
        defaultValue={[]}
        onChange={(e, n) => onChangeHandle(e, n, locations, setLocations)}
        renderInput={params => {
          return <>
            <InputText {...params}
              placeholder="Search Location"
              margin="none" />
            <StyledSvg xmlns="http://www.w3.org/2000/svg" width="14" height="18" viewBox="0 0 14 18">
              <g fill="none" fillRule="evenodd">
                <path d="M-5-3h24v24H-5z" />
                <path fill="#FF8896" fillRule="nonzero" d="M4.828 13.657a7 7 0 1 1 4.343 0L7 18l-2.172-4.343zM7 9a2 2 0 1 0 0-4 2 2 0 0 0 0 4z" />
              </g>
            </StyledSvg>
          </>
        }}
      />
    </InputCont>
  )
}

export default DropDown
