import React from 'react'
import {FormGroup,FormLabel,FormControlLabel,FormControl,Checkbox} from '@material-ui/core';
import styled from  'styled-components'

const StyledCheckbox = styled(Checkbox)`
  & .MuiSvgIcon-root{
    fill:#ff8896;
  }
`
const CheckBoxes = ({options, groupName, groupLabel}) => {

  const [state, setState] = React.useState({
    gilad: true,
    jason: false,
    antoine: false,
  });

  const { gilad, jason } = state;

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  return (
    <div>

      <FormControl component="fieldset">
        <FormLabel component="legend">Demo </FormLabel>
        <FormGroup>
          <FormControlLabel
            control={<StyledCheckbox checked={gilad} onChange={handleChange} name="gilad" />}
            label="Gilad Gray"
          />
          <FormControlLabel
            control={<StyledCheckbox checked={jason} onChange={handleChange} name="jason" />}
            label="Jason Killian"
          />
        </FormGroup>
      </FormControl>
  {/* 
      {
      options.map((option,index)=>{
        return <StyledCheckbox
        key={index}
        checked={checked}
        onChange={handleChange}
        inputProps={{ 'aria-label': 'primary checkbox' }}
        />
      })
    } 
  */}
    </div>
  )
}

export default CheckBoxes
