import React from 'react'
import {Link} from 'gatsby'
import Layout from "../../components/layout/layout"
import SEO from "../../components/seo"
import '../../components/layout/layout.scss'

import PrimaryButton from '../../components/buttonCont/buttonPrimary/buttonPrimary'
import SecondaryButton from '../../components/buttonCont/buttonSecondary/buttonSecondary'
import CloseButton from '../../components/buttonCont/closeButton/closeButton'
import PlayButton from '../../components/buttonCont/playButton/playButton'
import LoaderLogo from '../../components/loaderLogo/loaderLogo'
import TextLink from '../../components/textLink/textLink'
import Tags from '../../components/tags/tags'
import RadioButton from '../../components/radioButton/radioButton'
import Checkbox from '../../components/checkbox/checkbox'
import InputBox from '../../components/inputBox/inputBox'
import MultiSelect from '../../components/multiSelect/multiSelect'
import DropDown from '../../components/dropDown/dropDown'
import RangeSlider from '../../components/rangeSlider/rangeSlider'

const UiElements = () => {

  const options = [
    {
      label: 'Yes',
      value: 'yes'
    },
    {
      label: 'No',
      value: 'no'
    }
  ]

  return (
    <Layout row >
      <SEO title='Modal Component' />
      <article className='elements-container'>
        <h1 className='h1'>
          <a href='https://www.ui8.dev/'>Buttons</a>
          <Link to="/" className='dark link ctrl-btn'>Go back</Link> 
        </h1>

        <div className='flexStart row-sm'>
          <h3 className='h3'>Buttons:</h3>
          <PrimaryButton title='click 1' href='https://wwww.google.com'>Button 1</PrimaryButton>
          <SecondaryButton title='click 2' href='https://www.google.com' color='white'>Button 2</SecondaryButton>
        </div>
        <div className='flexStart row-sm'>
          <h3 className='h3'>Other Buttons:</h3>
          <CloseButton/>
          <PlayButton/>
        </div>
      </article>

      <article className='elements-container'>
        <h1 className='h1'>
          <a href='https://www.ui8.dev/'>Input Forms</a>
          <Link to="/" className='link ctrl-btn'>Go back</Link> 
        </h1>
        <main className='flex-column'>
          <aside className='flex'>
            <div className='flex-column row-sm'>
              <h3 className='h3'>Input</h3>
              <InputBox valid />
              <br/>
              <InputBox  />

            </div>

            <div className='flex-column row-sm evenly'>
              <h3 className='h3'>Drop Down</h3>
              <DropDown menuProps={
                {
                  button: 'Solutions',
                  links: ['/projects', '/', '/', '/'],
                  labels: ['Label 1', 'Label 2', 'Label 3', 'Label 4']
                }
              }
              />
            </div>
          </aside>

          <aside className='flex'>
            <div className='flex-column row-sm'>
              <h3 className='h3'>Multi Select</h3>
              <MultiSelect/>
            </div>

            <div className='flex-column row-sm'>
              <h3 className='h3'>Range Slider</h3>
              <RangeSlider/>
            </div>
          </aside>

        </main>

      </article>

      <article className='elements-container'>
        <h1 className='h1'>
          <a href='https://www.ui8.dev/'>Other Components</a>
          <Link to="/" className='link ctrl-btn'>Go back</Link> 
        </h1>
        <main className='flex'>
          <div className='flex-column row-sm'>
            <h3 className='h3'>Checkboxes</h3>
            <Checkbox options={options} groupName='GrpName' groupLabel='DemoLabel-1'/>
          </div>

          <div className='flex-column row-sm'>
            <h3 className='h3'>Radio Buttons</h3>
            <RadioButton options={options} groupName='GrpName' groupLabel='DemoLabel-1'/>
          </div>

          <div className='flex-column row-sm'>
            <h3 className='h3'>Tags</h3>
            <Tags/>
          </div>

          <div className='flex-column row-sm'>
            <h3 className='h3'>Loading</h3>
            <LoaderLogo/>
          </div>

          <div className='flex-column row-sm'>
            <h3 className='h3'>Text Link</h3>
            <TextLink href='/' label='Go to Home'/>
          </div>
        </main>
      </article>

    </Layout>
  )
}

export default UiElements
