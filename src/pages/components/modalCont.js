import React from 'react'
import Layout from "../../components/layout/layout"
import SEO from "../../components/seo"

import ModalLogin from '../../components/modalLogin/modalLogin'

const ModalCont = (props) => {
  return (
    <Layout>
      <SEO title='Modal Component' />
      <ModalLogin 
      label='Login' 
      href='https://influencers-api.f22labs.xyz/oauth' 
      heading='Woohoo! Welcome to Influensure!'
      subHeading='Influensure connects influencers with leading brands and agencies. Interested in joining?'
      />
    </Layout>
  )
}

export default ModalCont
