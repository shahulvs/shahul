import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout/layout"
import SEO from "../components/seo"

import '../page-styles/projects.scss'

const Nav = () => (
    <Layout>
    <SEO title="ui8.dev" />
    <article className='project'>
      <h1 className='h1'>
        <a href='https://www.ui8.dev/'>UI8</a>
        <Link to="/" className='link ctrl-btn'>Go back</Link> 
      </h1>
      <div className='link-cont'>
        <Link className='link' to="/components/checkout">Checkout Container</Link>
        <Link className='link' to="/components/heroContainer">HeroContainer</Link>
        <Link className='link' to="/components/pictureCont">Picture Container</Link>
        <Link className='link' to="/components/modalCont">Modal Login</Link>
        <Link className='link' to="/components/uiElements">UI Elements</Link>
      </div>
    </article>

  </Layout>
)

export default Nav
