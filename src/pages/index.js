import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout/layout"
import SEO from "../components/seo"

const IndexPage = () => {

  return (
    <Layout >
      <SEO title="Home" description='' />
       Work in progress...
       <br/>
       <br/>
       <Link className='link' to='/projects'>UI components</Link>
    </Layout>
  )
}

export default IndexPage
